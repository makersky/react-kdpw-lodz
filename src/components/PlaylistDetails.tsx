import React, { FC } from "react";
import { Playlist } from "../models/Playlist";

const playlist: Playlist = {
  id: 123,
  name: 'React Hits!',
  favorite: true,
  color: '#ff00ff'
}

type P = {
  playlist: Playlist | null,
  onEdit(): void
}

export const PlaylistDetails: FC<P> = React.memo(({ playlist, onEdit }) => (
  playlist ? <div>
    <dl>
      <dt>Name:</dt>
      <dd>{playlist.name}</dd>

      <dt>Favorite:</dt>
      <dd>{playlist.favorite ? 'Yes' : 'No'}</dd>

      <dt>Color</dt>
      <dd style={
        {
          color: playlist.color,
          backgroundColor: playlist.color,
        }
      }>{playlist.color}</dd>
    </dl>

    <input type="button" value="Edit" onClick={onEdit} />
  </div> : <p>Please select playlist</p>
))

PlaylistDetails.defaultProps = {
  playlist: playlist
}