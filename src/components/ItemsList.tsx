import React from "react";
import { Playlist } from "../models/Playlist";


type P = {
  playlists: Playlist[],
  selected: Playlist | null,
  onSelect(selected: Playlist): void
}

export class ItemsList extends React.Component<P> {
  render() {
    const { selected } = this.props

    return (
      <div className="list-group">
        {this.props.playlists.map((playlist, index) =>
          <div className={"list-group-item " + (selected && selected.id === playlist.id ? "active" : '')}
            key={playlist.id}

            onClick={() => this.props.onSelect(playlist)}>

            <span> {index + 1}. {playlist.name} </span>
          </div>
        )}
        {this.props.selected && this.props.selected.name}
      </div>
    )
  }
}