import React from "react";

type S = {
  query: string
}

type P = {
  onSearch(query: string): void
} & React.InputHTMLAttributes<HTMLInputElement>

export class SearchField extends React.Component<P, S>{

  state: S = {
    query: ''
  }

  search = () => {
    this.props.onSearch(this.state.query)
  }

  render() {
    return (
      <div className="input-group mb-3">

        {/* {...this.props} */}
        <input type="text"
          className="form-control"
          value={this.state.query}
          onChange={e => this.setState({
            query: e.currentTarget.value
          })}
          onKeyUp={e => e.key === "Enter" && this.search()} />

        <div className="input-group-append">
          <button className="btn btn-outline-secondary" onClick={this.search}>
            Search
          </button>
        </div>

      </div>)
  }
}