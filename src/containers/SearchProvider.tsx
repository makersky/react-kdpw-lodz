import { Album } from "../models/Album";
import React from "react";
import { searchCtx } from "../contexts";
import { searchMusic } from "../services";

type S = {
    query: string,
    results: Album[]

}

export class SearchProvider extends React.Component<{}, S> {
    state: S = {
        query: '', results: []
    }
    search = (query: string) => {
        this.setState({
            query
        })
        return searchMusic.searchAlbums(query).then(albums => {
            this.setState({
                results: albums
            })
            return albums
        })
    }

    render() {
        return <searchCtx.Provider value={{
            query: this.state.query,
            results: this.state.results,
            search: this.search
        }}>
            {this.props.children}
        </searchCtx.Provider>
    }
}