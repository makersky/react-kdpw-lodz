import { ItemsList } from "../components/ItemsList";
import { connect } from "react-redux";
import { Playlist } from "../models/Playlist";
import { playlistsSelect, selectedPlaylist } from "../reducers/playlists.reducer";

type AppState = {
    playlists: {
        query: string,
        items: Playlist[],
        selectedId: Playlist['id']
    }
}

export const PlaylistsList = connect(
    (state: AppState) => ({
        playlists: state.playlists.items,
        selected: selectedPlaylist(state)
    }),

    dispatch => ({
        onSelect(playlist: Playlist) {
            dispatch(playlistsSelect(playlist.id))
        }
    })

)(ItemsList)


// export function selectedPlaylist(state: {playlists:S}) {
//     return state.playlists.items.find(//
//         p => p.id === state.playlists.selectedId
//     ) || null
// }