import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { Playlist } from '../models/Playlist';
import { PlaylistDetails } from '../components/PlaylistDetails';
import { selectedPlaylist } from '../reducers/playlists.reducer';
// npm i --save @types/react-redux


type AppState = {
    playlists: {
        query: string,
        items: Playlist[],
        selectedId: Playlist['id'] | null
    }
}

const withPlaylist = connect(
    (state: AppState) => ({
        playlist: selectedPlaylist(state)
    })
)

export const SelectedPlaylist = withPlaylist(PlaylistDetails)


