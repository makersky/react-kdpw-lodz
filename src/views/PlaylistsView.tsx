import React from "react";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { ItemsList } from "../components/ItemsList";
import { Playlist } from "../models/Playlist";

type S = {
  query: string,
  mode: 'show' | 'edit',
  playlists: Playlist[],
  results: Playlist[],
  selected: Playlist | null
}

class PlaylistsView extends React.Component<{}, S> {
  state: S = {
    query: '',
    playlists: [

    ],
    results: [],
    mode: 'show',
    selected: null
  }

  constructor(props: {}) {
    super(props)
    this.state.selected = this.state.playlists[0]
  }

  select = (selected: Playlist) => {
    this.setState(
      prevState => ({
        selected: prevState.selected === selected ? null : selected
      })
    )
  }

  edit = () => {
    this.setState({
      mode: 'edit'
    })
  }

  cancel = () => {
    this.setState({
      mode: 'show'
    })
  }

  save = (draft: Playlist) => {
    this.setState(prevState => ({
      playlists: prevState.playlists.map(p => p.id === draft.id ? draft : p),
      selected: draft,
      mode: 'show'
    }))

    this.filterPlaylists(this.state.query)
  };

  filterPlaylists = (query: string) => {
    this.setState(prevState => ({
      query,
      results: prevState.playlists.filter(p => p.name.toLowerCase().includes(query.toLowerCase()))
    }))
  }


  componentDidUpdate() {

  }

  componentDidMount() {
    this.filterPlaylists(this.state.query)
    if (this.inputRef.current) {
      this.inputRef.current.focus()
    }
  }

  inputRef = React.createRef<HTMLInputElement>()

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            {/* 
            <input type="text"
              ref={this.inputRef}
              placeholder="Filter playlists"
              className="form-control mb-2"
              onKeyUp={event => this.filterPlaylists(event.currentTarget.value)} /> */}

            <ItemsList
              playlists={this.state.playlists}
              selected={this.state.selected}
              onSelect={this.select} />

          </div>
          <div className="col">

            {this.state.mode === 'show' &&
              <PlaylistDetails
                onEdit={this.edit}
                playlist={this.state.selected} />
            }

            {this.state.selected && this.state.mode === 'edit' &&
              <PlaylistForm
                playlist={this.state.selected}
                onCancel={this.cancel}
                onSave={this.save} />
            }

          </div>
        </div>
      </div>
    )
  }
}

export default PlaylistsView