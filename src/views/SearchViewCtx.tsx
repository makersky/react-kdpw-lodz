import React from "react"
import { searchCtx } from "../contexts"
import { SearchField } from "../components/SearchField"
import { SearchResults } from "../components/SearchResults"


export const SearchViewCtx: React.FC<{}> = () => {
    console.log('render')
    return ((<>
        <div className="row">
            <div className="col">

                <searchCtx.Consumer>
                    {(ctx) => <SearchField onSearch={ctx.search} />}
                </searchCtx.Consumer>

            </div>
        </div>
        <div className="row">
            <div className="col">

                <searchCtx.Consumer>
                    {(ctx) => <SearchResults results={ctx.results} />}
                </searchCtx.Consumer>

            </div>
        </div>
    </>))
}