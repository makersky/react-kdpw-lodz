import axios, { AxiosError } from 'axios'
import { AlbumsResponse } from '../models/Album'
import { auth } from '../services';

export class SearchService {

  constructor(private api_url: string) { }

  searchAlbums(query: string) {

    return axios.get<AlbumsResponse>(this.api_url, {
      params: {
        type: 'album',
        q: query
      }
    })
      .then(resp => resp.data.albums.items)
      .catch((err: any) => {

        if ((err as AxiosError).isAxiosError) {
          const error: AxiosError = err;

          if (error.response) {
            if (error.response.status === 401) {
              auth.authorize()
            }
            return Promise.reject(err.response.data.error.message)
          }
        }
        return Promise.reject(err)
      })

  }
}