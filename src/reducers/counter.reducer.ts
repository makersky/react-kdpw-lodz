
import { Reducer, Action, ActionCreator } from 'redux'

type S = number

export const counter: Reducer<S, Actions> = (state = 0, action) => {
    switch (action.type) {
        case 'INC': return state + action.payload;
        case 'DEC': return state - action.payload;
        default: return state
    }
}

type Actions = INC | DEC

interface INC extends Action<'INC'> {
    payload: number
}

interface DEC extends Action<'DEC'> {
    payload: number
}

export const inc: ActionCreator<INC> = (payload: number) => ({
    type: 'INC', payload
})

export const dec: ActionCreator<DEC> = (payload: number) => ({
    type: 'DEC', payload
})