import { Reducer, Action, ActionCreator } from "redux"
import { Playlist } from "../models/Playlist"


type S = {
    query: string,
    items: Playlist[],
    selectedId: Playlist['id'] | null
}

const initialState: S = {
    query: '',
    items: [{
        id: 123,
        name: 'React Hits!',
        favorite: true,
        color: '#ff00ff'
    },
    {
        id: 234,
        name: 'React Top20!',
        favorite: false,
        color: '#ffff00'
    },
    {
        id: 345,
        name: 'Best of React!',
        favorite: true,
        color: '#00ffff'
    }],
    selectedId: null
}

export const playlists: Reducer<S, Actions> = (
    state = initialState,
    action
) => {

    switch (action.type) {
        case 'PLAYLISTS_LOAD': return {
            ...state, items: action.payload
        };

        case 'PLAYLISTS_SELECT': return {
            ...state, selectedId: action.payload
        };

        case 'PLAYLISTS_FILTER': return {
            ...state, query: action.payload.query
        };

        case 'PLAYLISTS_UPDATE': {
            let updated = action.payload;
            return { ...state, item: state.items.map(p => p.id === updated.id ? updated : p) };
        }
        default: return state
    }
}

type Actions =
    | PLAYLISTS_LOAD
    | PLAYLISTS_UPDATE
    | PLAYLISTS_SELECT
    | PLAYLISTS_FILTER

interface PLAYLISTS_LOAD extends Action<'PLAYLISTS_LOAD'> {
    payload: Playlist[]
}

interface PLAYLISTS_UPDATE extends Action<'PLAYLISTS_UPDATE'> {
    payload: Playlist
}

interface PLAYLISTS_SELECT extends Action<'PLAYLISTS_SELECT'> {
    payload: Playlist['id']
}

interface PLAYLISTS_FILTER extends Action<'PLAYLISTS_FILTER'> {
    payload: { query: string }
}

export const playlistsLoad: ActionCreator<PLAYLISTS_LOAD> = (payload: Playlist[]) => ({
    type: 'PLAYLISTS_LOAD', payload
})

export const playlistsUpdate: ActionCreator<PLAYLISTS_UPDATE> = (payload: Playlist) => ({
    type: 'PLAYLISTS_UPDATE', payload
})

export const playlistsSelect: ActionCreator<PLAYLISTS_SELECT> = (payload: Playlist['id']) => ({
    type: 'PLAYLISTS_SELECT', payload
})

export const playlistsFilter: ActionCreator<PLAYLISTS_FILTER> = (query: string) => ({
    type: 'PLAYLISTS_FILTER', payload: { query }
})


export function selectedPlaylist(state: {
    playlists: S
}) {
    return state.playlists.items.find(//
        p => p.id === state.playlists.selectedId
    ) || null
}