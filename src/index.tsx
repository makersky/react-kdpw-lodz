import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { store } from './store';
import { Provider } from 'react-redux';

// import { HashRouter as Router } from "react-router-dom" // npm i react-router-dom @types/react-router-dom
import { BrowserRouter as Router } from "react-router-dom" // npm i react-router-dom @types/react-router-dom

(window as any).store = store;

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App />
        </Router>
    </Provider>
    , document.getElementById('root'));


// window.React = React
// window.ReactDOM = ReactDOM
// type Props = { item: string }

// const Item = (props: Props) => <p>Ala ma {props.item}</p>

// const items = ['kota', 'psa', 'rybki']

// let i = 0;
// setInterval(() => {

//     let div2 = <div id="123" title="placki" data-placki={i}>
//         <Item item={'Placki'}></Item>
//         {items.map(item => <Item item={item} key={item} />)}
//         <p>{i++}</p>
//     </div>


//     ReactDOM.render(div2, (window as any).root)

// }, 500)



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
