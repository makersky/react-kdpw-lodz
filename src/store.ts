import { createStore, combineReducers } from 'redux'
import { counter } from './reducers/counter.reducer'
import { playlists } from './reducers/playlists.reducer'
import { Playlist } from './models/Playlist'

export type AppState = {
    playlists: {
        query: string,
        items: Playlist[],
        selectedId: Playlist['id'] | null
    },
    counter: number
}


const reducer = combineReducers({
    counter,
    playlists
})

export const store = createStore(reducer)

