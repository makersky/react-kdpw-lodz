import { SearchService } from "./services/SearchService";
import { AuthService } from "./services/AuthService";
import axios from 'axios';

export const searchMusic = new SearchService(
    'https://api.spotify.com/v1/search'
)

export const auth = new AuthService(
    'https://accounts.spotify.com/authorize',
    '70599ee5812a4a16abd861625a38f5a6',
    'http://localhost:3000/'
)

// auth.getToken()

axios.interceptors.request.use((req)=>{
    req.headers['Authorization'] = `Bearer ${auth.getToken()}`
    return req
})


// placki@placki.com
// ******