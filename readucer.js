inc = { type: 'INC', payload: 1 };
dec = { type: 'DEC', payload: 1 };
addTodo = todo => ({ type: 'ADD_TODO', payload: todo });

const counterReducer = (state = 0, action) => {
    switch (action.type) {
        case "INC": return state + action.payload
        case "DEC": return state - action.payload
        default: return state;
    }
}

const reducer = (state, action) => {
    switch (action.type) {
        // case "INC": return { ...state, counter: state.counter + action.payload }
        // case "DEC": return { ...state, counter: state.counter - action.payload }
        case "ADD_TODO": return { ...state, todos: [...state.todos, action.payload] }
        // default: return state;
        default: return { ...state, counter: counterReducer(state.counter, action) }
    }
}

[inc, inc, addTodo('placki'), dec, inc].reduce(reducer, {
    counter: 0, todos: []
})


